FROM python:3.6-slim

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
ENV MAX_THREADS 5
ENV DEFAULT_TIMEOUT 60
ENV MASTER_NODES=""
ENV APP_PORT=8080
ENV MAX_PAGES_SCRAPPING=5
# Python, don't write bytecode!
ENV PYTHONDONTWRITEBYTECODE 1

RUN apt-get -y update
RUN apt-get -y install gcc build-essential pipenv nano wget curl file git firefox-esr apt-utils

# Install geckodriver
WORKDIR /tmp

RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.25.0/geckodriver-v0.25.0-linux64.tar.gz
RUN tar -xvf geckodriver-v0.25.0-linux64.tar.gz
RUN chmod +x geckodriver
RUN mv geckodriver /usr/local/bin/
RUN rm -f geckodriver-v0.25.0-linux64.tar.gz

VOLUME /app /data

WORKDIR /app

ENTRYPOINT pipenv install ; pipenv run python __init__.py