import os
import time
from threading import Thread
from time import sleep
from typing import List, Union
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

import Models


class SiteFactory:
    _site_instances: List['Models.Site'] = []

    @staticmethod
    def normalize_instance_site_tree(data: Union[dict, list]) -> Union['Models.Site', list]:
        if type(data) == list:
            returned_data = None
        else:
            returned_data: Models.Site = Models.Site.normalize_instanciate(data)

        return returned_data


class HttpRequestFactory:
    _pages_enqueued: List['Models.Page'] = []

    @staticmethod
    def enqueue(page: 'Models.Page'):
        HttpRequestFactory._pages_enqueued.append(page)

    @staticmethod
    def get_queued_pages_content():
        running_threads: List[Thread] = []

        while True:
            if len(HttpRequestFactory._pages_enqueued) > 0:
                if len(running_threads) < int(os.environ['MAX_PAGES_SCRAPPING']):
                    page = HttpRequestFactory._pages_enqueued[0]

                    page_download_content_thread = Thread(
                        target=HttpRequestFactory._download_and_set_page_content_and_remove_from_queue,
                        args=(page,)
                    )

                    HttpRequestFactory._pages_enqueued.remove(page)

                    running_threads.append(page_download_content_thread)

                    page_download_content_thread.start()

            for thread in running_threads:
                if not thread.isAlive():
                    running_threads.remove(thread)

            if len(HttpRequestFactory._pages_enqueued) == 0 and len(running_threads) == 0:
                break

            sleep(2)

    @staticmethod
    def _download_and_set_page_content_and_remove_from_queue(page: 'Models.Page'):
        try:
            options = Options()
            options.add_argument('--ignore-certificate-errors')
            options.add_argument('--incognito')
            options.add_argument('--headless')

            browser = webdriver.Firefox(options=options)

            # TODO: Deal with HTTP and Timeout errors
            browser.set_page_load_timeout(int(os.environ['DEFAULT_TIMEOUT']))

            browser.get(page.get_url())

            SCROLL_PAUSE_TIME = 0.5

            # Get scroll height
            last_height = browser.execute_script("return document.body.scrollHeight")

            while True:
                # Scroll down to bottom
                browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

                # Wait to load page
                time.sleep(SCROLL_PAUSE_TIME)

                # Calculate new scroll height and compare with last scroll height
                new_height = browser.execute_script("return document.body.scrollHeight")

                if new_height == last_height:
                    break

                last_height = new_height

            page.set_content(browser.page_source)

            if page in HttpRequestFactory._pages_enqueued:
                HttpRequestFactory._pages_enqueued.remove(page)

            browser.quit()
        except Exception as e:
            print("##############ERROR", e)