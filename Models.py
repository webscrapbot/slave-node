from typing import List, Dict

from bs4 import BeautifulSoup

import Factories


class Site:
    id: int
    domain: str
    secure: bool
    pages: List["Page"]
    scrap_info_patterns: List["ScrapPattern"]

    def __init__(self, domain: str, secure: bool, id=None, pages=None, scrap_info_patterns=None):
        if pages is None:
            pages = []

        if scrap_info_patterns is None:
            scrap_info_patterns = []

        self.domain = domain
        self.secure = secure
        self.pages = pages
        self.scrap_info_patterns = scrap_info_patterns
        self.id = id

    def get_full_url(self) -> str:
        if self.secure:
            return 'https://' + self.domain + '/'

        return 'http://' + self.domain + '/'

    def get_pages_content(self):
        for page in self.pages:
            Factories.HttpRequestFactory.enqueue(page)

        Factories.HttpRequestFactory.get_queued_pages_content()

    @staticmethod
    def normalize_instanciate(data: dict) -> "Site":
        # Throw exception case domain or secure not in data
        instance: "Site" = Site(domain=data['domain'], secure=data['secure'])

        if 'id' in data:
            instance.id = data['id']
        if 'pages' in data:
            for page in data['pages']:
                page['site'] = instance

                instance.pages.append(Page.normalize_instanciate(page))

        if 'scrap_patterns' in data:
            for scrap_info_pattern in data['scrap_patterns']:
                instance.scrap_info_patterns.append(ScrapPattern.normalize_instanciate(scrap_info_pattern))

        return instance


class Page:
    id: int
    path: str
    _content: str = None
    site: Site

    def __init__(self,
                 path: str,
                 site: Site,
                 id: int = None
                 ):
        self.path = path
        self.site = site
        self.id = id

    def get_content(self):
        if self._content:
            return self._content

        Factories.HttpRequestFactory.enqueue(self)

        Factories.HttpRequestFactory.get_queued_pages_content()

        return self.get_content()

    def set_content(self, content: str):
        self._content = content

    def crawl(self) -> List['Page']:
        pages: List['Page'] = []

        for link in BeautifulSoup(self.get_content(), 'html.parser').select('a'):
            link_href: str = link.get('href')

            if self.is_a_valid_path(link_href):
                path = self.get_path(link_href)

                path_founded = False

                for page in pages:
                    if page.path == path:
                        path_founded = True

                if not path_founded:
                    pages.append(
                        Page(
                            path,
                            self.site
                        )
                    )

        return pages

    def scrap(self) -> List[Dict[int, List['Info']]]:
        container_infos: List[Dict[int, List['Info']]] = []

        for scrap_pattern in self.site.scrap_info_patterns:
            for container in BeautifulSoup(self.get_content(), 'html.parser').select(scrap_pattern.container.pattern):

                container_data: Dict[int, List['Info']] = {
                    "id": scrap_pattern.container.id,
                    "infos": []
                }

                for info_pattern in scrap_pattern.infos_pattern:
                    info_result_count = 0

                    result_info = None

                    if info_pattern.pattern[0] == '^':
                        info_element = container
                        if info_pattern.required_attribute == "text":
                            result_info = container.text
                        else:
                            result_info = container[info_pattern.required_attribute]
                    else:
                        for info_element in container.select(info_pattern.pattern):
                            if info_pattern.required_attribute == "text":
                                result_info = info_element.text
                            else:
                                result_info = info_element[info_pattern.required_attribute]

                    container_data.get('infos').append(Info(scrap_info_pattern=info_pattern, result=result_info))

                    info_result_count += 1

                    if info_pattern.results_limit != 0 and info_pattern.results_limit == info_result_count:
                        break
                container_infos.append(container_data)

        return container_infos

    def get_path(self, url: str) -> str:
        url = url.replace(self.site.get_full_url(), '')

        while url.startswith('/'):
            url = url[1:]

        while url.endswith('/'):
            url = url[:-1]

        return url

    def is_a_valid_path(self, path: str) -> bool:
        if path is not None:
            return (path.find(self.site.get_full_url()) >= 0 and path != self.site.get_full_url()) \
                   or (path[0] == "/") if len(path) > 0 else False

        return False

    def get_url(self) -> str:
        return self.site.get_full_url() + (self.path if self.path != '/' else '')

    @staticmethod
    def normalize_instanciate(data: dict) -> 'Page':
        #         Throw exception case path and site are not in data
        if 'path' in data and 'site' in data:
            instance = Page(path=data['path'], site=data['site'])

            if 'id' in data:
                instance.id = data['id']

            return instance
        else:
            raise Exception("Path or site are not set in path data")


class ScrapPattern:
    id: int
    container: "ContainerScrapPattern"
    infos_pattern: List['InfoScrapPattern']

    def __init__(self, container: "ContainerScrapPattern", id: int = None, info_scrap_patterns=None):
        if info_scrap_patterns is None:
            info_scrap_patterns = []

        self.container = container
        self.infos_pattern = info_scrap_patterns
        self.id = id

    @staticmethod
    def normalize_instanciate(data: dict) -> 'ScrapPattern':
        container = ContainerScrapPattern.normalize_instanciate(data=data['container'])

        info_scrap_patterns = []

        for info_scrap_pattern in data['infos_scrap_pattern']:
            info_scrap_patterns.append(InfoScrapPattern.normalize_instanciate(data=info_scrap_pattern))

        instance = ScrapPattern(container=container, info_scrap_patterns=info_scrap_patterns)

        if 'id' in data:
            instance.id = data['id']

        return instance


class ContainerScrapPattern:
    id: int
    pattern: str

    def __init__(self, pattern: str, id: int = None):
        self.pattern = pattern
        self.id = id

    @staticmethod
    def normalize_instanciate(data) -> "ContainerScrapPattern":
        instance = ContainerScrapPattern(pattern=data['pattern'], id=data['id'])

        if 'id' in data:
            instance.id = data['id']

        return instance


class InfoScrapPattern:
    id: int
    title: str
    pattern: str
    required_attribute: str
    results_limit: int

    def __init__(self, title: str, pattern: str, required_attribute: str, results_limit: int, id: int = None):
        self.title = title
        self.pattern = pattern
        self.required_attribute = required_attribute
        self.results_limit = results_limit
        self.id = id

    @staticmethod
    def normalize_instanciate(data):
        instance = InfoScrapPattern(
            title=data['title'],
            pattern=data['pattern'],
            required_attribute=data['required_attribute'],
            results_limit=data['results_limit']
        )

        if 'id' in data:
            instance.id = data['id']

        return instance


class Info:
    scrap_info_pattern: "InfoScrapPattern"
    result: str

    def __init__(self, scrap_info_pattern: "InfoScrapPattern", result: str):
        self.scrap_info_pattern = scrap_info_pattern
        self.result = result
