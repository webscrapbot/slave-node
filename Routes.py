import json
from typing import List

from bottle import route, request, response, HTTPResponse

import Schemas
import Models
import Factories


def define_routes():
    @route("/status", method="GET")
    def status():
        return {
            "data": []
        }

    @route('/crawl', method="POST")
    def crawl():
        sites: List[Models.Site] = Schemas.SiteSchema().load(request.json['sites'], many=True)

        for site in sites:
            site.get_pages_content()

        response.content_type = 'application/json'

        return {"data": Schemas.SiteSchema().dump(sites, many=True)}

    @route("/scrap", method="POST")
    def scrap():
        try:
            if 'data' in request.json:
                if type(request.json['data']) == list:
                    normalized_instanciated_sites = Factories.SiteFactory.normalize_instance_site_tree(request.json['data'])

                    for normalized_instanciated_site in normalized_instanciated_sites:
                        normalized_instanciated_site.get_pages_content()

                        for page in normalized_instanciated_site.pages:
                            page.scrap()

                    data = Schemas.SiteSchema().dump(normalized_instanciated_sites, many=True)

                else:
                    normalized_instanciated_site = Factories.SiteFactory.normalize_instance_site_tree(request.json['data'])

                    normalized_instanciated_site.get_pages_content()

                    for page in normalized_instanciated_site.pages:
                        page.scrap()

                    data = Schemas.SiteSchema().dump(normalized_instanciated_site)

                response.content_type = 'application/json'

                return {"data": data}
            else:
                # TODO: Create class to group exceptions
                raise HTTPResponse(
                    json.dumps(
                        dict(
                            code="rewitdat",  # Request Without Data
                            data=dict(
                                statement="",
                                message="Request without data"
                            )
                        )
                    ),
                    code=400,
                    status=400,
                    headers={
                        "Content-Type": "application/json"
                    }
                )
        except Exception as e:
            print(e)
            raise HTTPResponse(
                json.dumps(
                    dict(
                        code="error",  # Request Without Data
                        data=dict(
                            statement="",
                            message="Exception"
                        )
                    )
                ),
                code=400,
                status=400,
                headers={
                    "Content-Type": "application/json"
                }
            )