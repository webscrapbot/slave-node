from typing import List, Dict

from marshmallow import Schema, post_load, fields

from Factories import SiteFactory
import Models


class SiteSchema(Schema):
    id: int = fields.Integer(default=fields.missing_)
    domain: str = fields.Str()
    secure: bool = fields.Bool()
    pages: List["Models.Page"] = fields.Nested(
        'Schemas.PageSchema',
        many=True,
    )
    scrap_info_patterns: List["Models.ScrapInfoPattern"] = fields.Nested(
        "Schemas.ScrapPatternSchema",
        many=True
    )

    @post_load
    def make_site(self, data, **kwargs):
        return SiteFactory.get_site(**data)


class PageSchema(Schema):
    id: int = fields.Integer(default=fields.missing_)
    path: str = fields.Str()
    site: "Models.Site" = fields.Nested(
        'Schemas.SiteSchema',
        many=False,
        only=("domain", "secure",)
    )
    path_crawled_pages: List['Models.Page'] = fields.Method("get_path_crawled_pages")
    scrapped_infos: List['Models.Page'] = fields.Method("get_scrapped_infos")

    def get_path_crawled_pages(self, page: 'Models.Page') -> List[str]:
        path_crawled_pages: List[str] = []

        for crawled_page in page.crawl():
            path_crawled_pages.append(crawled_page.path)

        return path_crawled_pages

    def get_scrapped_infos(self, page: 'Models.Page') -> List[Dict[int, List[dict]]]:
        containers_data: List[Dict[int, List[dict]]] = []

        for scrapped_container_infos in page.scrap():
            container_data: Dict[int, List[dict]] = {
                'id': scrapped_container_infos['id'],
                'scrapped_info_datas': []
            }

            for scrapped_info in scrapped_container_infos['infos']:
                container_data['scrapped_info_datas'].append(
                    dict(
                        info=scrapped_info.result,
                        info_pattern_id=scrapped_info.scrap_info_pattern.id,
                    )
                )

            containers_data.append(container_data)

        return containers_data

    @post_load
    def make_page(self, data, **kwargs):
        return Models.Page(**data)

    class ScrapPatternSchema(Schema):
        id: int = fields.Integer(default=fields.missing_)
        container = fields.Nested(
            "Schemas.ContainerScrapPatternSchema",
            many=False
        )
        infos_pattern = fields.Nested(
            "Schemas.InfoScrapPatternSchema",
            many=True
        )

        @post_load
        def make_scrap_pattern(self, data, **kwargs):
            return Models.ScrapPattern(**data)

    class ContainerScrapPatternSchema(Schema):
        id: int = fields.Integer(default=fields.missing_)
        pattern = fields.Str()

        @post_load
        def make_scrap_container_pattern(self, data, **kwargs):
            return Models.ContainerScrapPattern(**data)

    class InfoScrapPatternSchema(Schema):
        id: int = fields.Integer(default=fields.missing_)
        title = fields.Str()
        pattern = fields.Str()
        required_attribute = fields.Str()
        results_limit = fields.Int()

        @post_load
        def make_scrap_info_pattern(self, data, **kwargs):
            return Models.InfoScrapPattern(**data)
