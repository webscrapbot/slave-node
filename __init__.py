import json
import os
import time
from typing import List

import requests
import yaml
from bottle import run, install, hook, response
from bottle_swagger import SwaggerPlugin

from Routes import define_routes


# TODO: Log everything in file
# TODO: Log all access

def register_as_slave_node():
    """
    Register this node as a slave for master nodes defined in environments
    :return: No
    """

    hosts: List[str] = os.environ['MASTER_NODES'].split(',')

    for host in hosts:
        keep_trying = True

        while keep_trying:
            try:
                register_slave_request = requests.post(
                    'http://' + host + "/register_slave_node",
                    json.dumps({
                        'data': {
                            'isASlaveNode': True,
                            'port': os.environ['APP_PORT'],
                            'max_pages_scraping': os.environ['MAX_PAGES_SCRAPPING']
                        }
                    }),
                    headers={
                        'content-type': 'application/json'
                    }
                )

                if 400 <= register_slave_request.status_code < 500:
                    if register_slave_request.status_code == 409:
                        print("Already registered")
                    else:
                        # Error in this side
                        print("Request error")
                else:
                    # Error in server's side
                    print("Server error")

                print(register_slave_request.content)

                keep_trying = False
            except Exception as e:
                print("########################", e)

                time.sleep(1)


@hook('after_request')
def enable_cors_set_content_type():
    """
    You need to add some headers to each request.
    Don't use the wildcard '*' for Access-Control-Allow-Origin in production.
    """
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
    response.headers['Content-Type'] = 'application/json'


define_routes()

# TODO: Log if could not register
register_as_slave_node()

# this_dir = os.path.dirname(os.path.abspath(__file__))
# with open("{}/swagger.yml".format(this_dir)) as f:
#    swagger_def = yaml.load(f, Loader=yaml.FullLoader)

# install(SwaggerPlugin(swagger_def))

run(reloader=True, host='0.0.0.0', port=8080)
